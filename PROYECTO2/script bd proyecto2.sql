CREATE SCHEMA `bd_proyecto2` 
DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci ;

USE bd_proyecto2;

CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID de cada producto',
  `tipo` varchar(11) NOT NULL COMMENT 'Tipo del producto (Herramienta o Material)',
  `nombre` varchar(45) NOT NULL COMMENT 'Nombre del producto',
  `medida` varchar(20) DEFAULT '- sin medida -' COMMENT 'Medida en caso de que el producto sea un material (en milimetros o en pulgadas) (Herramienta sera "sin medida")',
  `capacidad` varchar(20) DEFAULT '- sin capacidad -' COMMENT 'Capacidad en caso de que el producto sea una herramienta (Tabrajo liviano, mediano, pesado) (Material sera "sin capacidad")',
  `precio` double NOT NULL COMMENT 'Precio del producto (Unitario en caso de herramienta, por kilo en materiales)',
  `cantidad` INT NULL DEFAULT 0 COMMENT 'cantidad que hay de dicho producto en el inventario',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) 
ENGINE=InnoDB
COMMENT='Tabla con los datos del inventario completo de la ferreteria.\nvariables:\n	- ID - Tipo - Nombre - Medida - Capacidad - Precio -';

CREATE TABLE `bd_proyecto2`.`factura` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico de la factura ',
  `nombre_cliente` VARCHAR(45) NOT NULL COMMENT 'nombre del cliente ',
  `precio_total` DOUBLE NOT NULL COMMENT 'total a pagar de dicha factura',
  PRIMARY KEY (`id`))
COMMENT = 'Tabla que contiene las facturas de la ventas de la ferreteria';

CREATE TABLE `bd_proyecto2`.`detalle_factura` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_id_factura` INT NOT NULL,
  `nombre_producto` VARCHAR(45) NOT NULL,
  `precio` DOUBLE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_factura_idx` (`fk_id_factura` ASC) VISIBLE,
  CONSTRAINT `fk_factura`
    FOREIGN KEY (`fk_id_factura`)
    REFERENCES `bd_proyecto2`.`factura` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
INSERT INTO bd_proyecto2.producto (`tipo`, `nombre`, `medida`, `capacidad`, `precio`, `cantidad`) 
 VALUES ('Herramienta', 'Martillo', '(sin medida)', 'Ligero', 2000, 10),
		('Material', 'Tornillo Gibson', '2" 1/2', '(sin capacidad)', 800, 300),
        ('Herramienta', 'Taladro ', '(sin medida)', 'Mediano', 20000, 10),
        ('Herramienta', 'Sierra Circular', '(sin medida)', 'Pesado', 40000, 10),
        ('Material', 'Tornillo Gibson', '30mm', '(sin capacidad)', 800, 350),
        ('Material', 'Clavo acero', '3"', '(sin capacidad)', 350, 500),
        ('Material', 'Clavo Galvanizado', '52 mm', '(sin capacidad)', 450, 300),
        ('Herramienta', 'Esmeriladora', '(sin medida)', 'Mediano', 15000, 10),
        ('Herramienta', 'Mazo de Hierro', '(sin medida)', 'Pesado', 5000, 10),
        ('Material', 'Tornillo Carroceria', '5"', '(sin capacidad)', 1000, 200),
        ('Herramienta', 'Serrucho', '(sin medida)', 'ligero', 4000, 10),
        ('Herramienta', 'Sierra Caladora', '(sin medida)', 'Mediano', 25000, 10);
        
