package proyecto2;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import proyecto2.control.ControlProyecto;
import proyecto2.modelo.DAO.ProductoDAO;
import proyecto2.vista.Ferreteria;

/**
 *
 * @author Matthew Miranda
 */
public class PROYECTO2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Iniciando aplicacion..");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());            
            JFrame.setDefaultLookAndFeelDecorated(true);
            ProductoDAO pd = ProductoDAO.obtenerInstancia();
            pd.listar();            
        } catch (ClassNotFoundException
                | IllegalAccessException
                | InstantiationException
                | UnsupportedLookAndFeelException
                | SQLException e) {
            System.err.printf("Excepción: '%s'%n", e.getMessage());
        }

        mostrarInterfaz();

    }

    public static void mostrarInterfaz() {
        SwingUtilities.invokeLater(() -> {
            try {
                System.out.println("Abriendo interfaz grafica.");
                new Ferreteria("PROYECTO 2", new ControlProyecto()).inicializar();
            } catch (SQLException ex) {
                Logger.getLogger(PROYECTO2.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

}
