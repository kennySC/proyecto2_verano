package proyecto2.control;

import java.sql.SQLException;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto2.modelo.ModeloFacturas;
import proyecto2.modelo.ModeloProductos;
import proyecto2.modelo.entidades.DetalleFactura;
import proyecto2.modelo.entidades.Factura;
import proyecto2.modelo.entidades.ListaDetallesFactura;
import proyecto2.modelo.entidades.ListaFacturas;
import proyecto2.modelo.entidades.ListaProductos;
import proyecto2.modelo.entidades.Producto;

/**
 *
 * @author Matthew Miranda
 */
public class ControlProyecto {

    private ModeloProductos datosProductos;
    private ModeloFacturas datosFacturas;
    

    public ControlProyecto(ModeloProductos datos) {
        this.datosProductos = datos;
        try {
            datosFacturas = new ModeloFacturas();
        } catch (SQLException ex) {
            Logger.getLogger(ControlProyecto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ControlProyecto() throws SQLException {
        this(new ModeloProductos());
    }

    public void registrar(Observer nuevoObservador) {
        System.out.printf("Registrando: %s..%n", nuevoObservador);
        datosProductos.addObserver(nuevoObservador);
    }

    public void suprimir(Observer actual) {
        System.out.printf("Suprimiendo: %s..%n", actual);
        datosProductos.deleteObserver(actual);
    }

    public void cerrarAplicacion() {
        System.out.println("Aplicación finalizada exitosamente..");
        System.exit(0);
    }
    
    // <editor-fold defaultstate="collapsed" desc=" Metodos relacionados a productos ">    
    
    public String agregarProducto(Producto p) throws SQLException {
        return datosProductos.agregarProducto(p);
    }

    public String eliminarProducto(int id) throws SQLException {
        return datosProductos.eliminarRegistro(id);
    }

    public ListaProductos obtenerListaProductos() {
        return datosProductos.obtenerLista();
    }

    public String actualizarProducto(Producto producto) throws SQLException {
        return datosProductos.actualizarProducto(producto);
    }

    public String eliminarTODOSProductos() throws SQLException {
        return datosProductos.eliminarTODOS();
    }
    
    public void buscarProducto(String criterio, String parametro) throws SQLException{
        datosProductos.buscar(criterio, parametro);
    }

    public void refrescarDatos() throws SQLException {
        datosProductos.refrescarDatos();
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Metodos realcionados a facturas ">
    
    public void registrarObservadorFacturas(Observer observador){
        datosFacturas.addObserver(observador);
    }
    
    public void suprimirObservadorFacturas(Observer o){
        datosFacturas.deleteObserver(o);
        datosFacturas.limpiar();
    }
    
    public ListaFacturas obtenerFacturas(){
        return datosFacturas.obtenerFacturas();
    }
    
    public ListaDetallesFactura obtenerDetallesFactura(){
        return datosFacturas.obtenerDetalles();
    }
    
    public void buscarFactura(String nombre) throws SQLException{
        datosFacturas.buscarFactura(nombre);
    }
    
    public int agregarFactura(Factura f) throws SQLException{
        return datosFacturas.agregarFactura(f);
    }
    
    public void agregarDetalleFactura(DetalleFactura df){
        datosFacturas.agregarDetalleFatura(df);
    }
    
    public boolean guardarDetallesFactura(int idFK) throws SQLException{
        return datosFacturas.guadarDetallesFactura(idFK);
    }
    
    public void quitarDetalle(DetalleFactura df){
        datosFacturas.quitarDetalle(df);
    }
    
    public double obtenerPrecioTotal() {
        return datosFacturas.obtenerPrecioTotal();
    }

    public void setFacturando(boolean b) {
        datosFacturas.setFacturando(b);
    }
    
    public boolean getFacturando(){
        return datosFacturas.getFacturando();
    }

    // </editor-fold>    
    
}
