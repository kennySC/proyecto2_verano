package proyecto2.vista;

import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/**
 *
 * @author Matthew Miranda
 */
public class BarraEstado extends JPanel {

    public BarraEstado() {
        super();
        configurar();
    }

    private void configurar() {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        add(etqEstado = new JLabel());
    }

    public void init() {
        mostrarMensaje();
    }

    public void mostrarMensaje(String msj) {
        etqEstado.setText(String.format("%s ", msj));
    }

    public void mostrarMensaje() {
        mostrarMensaje(null);
    }

    private JLabel etqEstado;
}
