package proyecto2.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import proyecto2.control.ControlProyecto;

/**
 *
 * @author Matthew Miranda
 */
public class VentanaAplicacion extends JFrame implements Observer {

    private final ControlProyecto gestorPrincipal;
    private JScrollPane controlDesplazamientoMapa;
    private BarraEstado barraEstado;
    private JMenuBar menuPrincipal;
    private JMenu archivo;
    private JMenu datos;
    private JMenu informacion;
    private JMenuItem itemDatos;
    private JMenuItem itemSalir;
    private JMenuItem itemCreditos;
    private JButton agregar;
    private JButton eliminar;
    private JButton buscar;
    private JButton actualizar;

    public VentanaAplicacion(String titulo, ControlProyecto nuevoGestor) {
        super(titulo);
        this.gestorPrincipal = nuevoGestor;
        configurarVentana();
    }

    private void configurarVentana() {
        ajustarComponentes(getContentPane());
        setResizable(true);
        setSize(800, 650);
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(640, 480));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cerrarAplicacion();
            }
        });
    }

    public void ajustarComponentes(Container c) {
        ajustarMenu();
        c.setLayout(new BorderLayout());
        JPanel principal = new JPanel();

        JPanel botones = new JPanel(new GridBagLayout());
        JPanel botonesUP = new JPanel(new GridBagLayout());
        JPanel botonesDown = new JPanel(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
               
        gbc.insets = new Insets(1, 0, 10, 0);
        gbc.ipady = 5;
        
        gbc.gridx = 0;
        gbc.gridy = 0;                

        botonesUP.add(new JLabel("Modos"), gbc);
        
        gbc.gridy = 1;
        botonesUP.add(agregar = new JButton("Agregar"), gbc);

        gbc.gridy = 2;
        botonesUP.add(actualizar = new JButton("Actualizar"), gbc);

        gbc.gridy = 3;
        botonesUP.add(eliminar = new JButton("Eliminar"), gbc);

        gbc.gridy = 4;
        botonesUP.add(buscar = new JButton("Buscar"), gbc);
        
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.weighty = 0.1;
        botones.add(botonesUP, gbc);
        
//        botones.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2, true));
        botones.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        
        c.add(BorderLayout.LINE_END, botones);
        
        principal.setLayout(new BorderLayout());
        c.add(BorderLayout.PAGE_END, barraEstado = new BarraEstado());
        barraEstado.mostrarMensaje("Comedme los huevos");
    }

    public void ajustarMenu() {
        menuPrincipal = new JMenuBar();
        menuPrincipal.add(archivo = new JMenu("Archivo"));
        menuPrincipal.add(datos = new JMenu("Datos"));
        menuPrincipal.add(informacion = new JMenu("Informacion"));
        datos.add(itemDatos = new JMenuItem("Esa puta maire"));
        informacion.add(itemCreditos = new JMenuItem("Creditos"));
        archivo.add(itemSalir = new JMenuItem("Salir"));                
    }

    public void cerrarAplicacion() {
        gestorPrincipal.cerrarAplicacion();
    }

    public void inicializarVentana() {
        setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Puros corridos tumbaos chingones");
    }

    private void creditos() {
        JOptionPane.showMessageDialog(null, "Proyecto desarrollado por:\n\n"
                + "\tMatthew Miranda Araya\n"
                + "\tKenneth Sibaja Castro");
    }

}
