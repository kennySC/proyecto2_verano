package proyecto2.vista;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import proyecto2.control.ControlProyecto;
import proyecto2.modelo.entidades.DetalleFactura;
import proyecto2.modelo.entidades.Producto;
import proyecto2.modelo.tablas.ModeloColumnasProductos;
import proyecto2.modelo.tablas.ModeloTablaProductos;

/**
 *
 * @author Matthew Miranda
 */
public class Ferreteria extends javax.swing.JFrame {

    private final ControlProyecto gestorPrincipal;
    private JScrollPane controlDesplazamientoTabla;
    private BarraEstado barraEstado;
    private ModeloTablaProductos modeloTP;
    private String modo;
    private int filaSelecionada;
    private Facturar ventanaFacturar;
    private Producto productoAux;

    /**
     * Creates new form Ferreteria
     */
    public Ferreteria(String titulo, ControlProyecto gp) {
        super(titulo);
        modo = "espera";
        gestorPrincipal = gp;
        initComponents();
        configurar();
    }

    public void inicializar() {
        setVisible(true);
    }

    public void configurar() {
        ajustarComponentes();
    }

    public void ajustarComponentes() {

        deshabilitarRB();
        bgModoFiltrar.add(rbTipo);
        bgModoFiltrar.add(rbNombre);

        //  Botones        
        jpBarraEstado.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.weightx = 0.1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        jpBarraEstado.add(barraEstado = new BarraEstado(), gbc);
        barraEstado.mostrarMensaje("PRUEBA");

        jtTablaProductos.setModel(modeloTP = new ModeloTablaProductos(gestorPrincipal.obtenerListaProductos()));
        jtTablaProductos.setColumnModel(new ModeloColumnasProductos());

        gestorPrincipal.registrar(modeloTP);

        jtTablaProductos.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selecionarElemento(jtTablaProductos.rowAtPoint(e.getPoint()));
            }
        });
    }

    public void cerrarAplicacion() {
        gestorPrincipal.cerrarAplicacion();
    }

    private void creditos() {
        JOptionPane.showMessageDialog(null, "Proyecto desarrollado por:\n\n"
                + "\tMatthew Miranda Araya\n"
                + "\tKenneth Sibaja Castro");
    }

    public void habilitarBotones() {
        btnAgregar.setEnabled(true);
        btnBuscar.setEnabled(true);
        btnModificar.setEnabled(true);
        btnEliminar.setEnabled(false);
        btnFacturar.setEnabled(false);
        jtTablaProductos.clearSelection();
    }

    public void deshabilitarCampos() {
        txtNomProducto.setEnabled(false);
        txtCantidad.setEnabled(false);
        txtPrecio.setEnabled(false);
        cmbTipo.setEnabled(false);
        cmbCapacidad.setEnabled(false);
    }

    public void habilitarCampos() {
        txtNomProducto.setEnabled(true);
        txtCantidad.setEnabled(true);
        txtPrecio.setEnabled(true);
        cmbTipo.setEnabled(true);
    }

    public void selecionarElemento(int fila) {
        filaSelecionada = fila;
        productoAux = modeloTP.getEntidadEnFila(fila);
        if (modo.equalsIgnoreCase("actualizar")) {
            productoACampos();
            habilitarCampos();
        } else if (modo.equalsIgnoreCase("espera")) {
            btnEliminar.setEnabled(true);
            btnFacturar.setEnabled(true);
        }
    }

    private void eliminarTODOSProductos() {
        if (JOptionPane.showConfirmDialog(this,
                "Desea eliminar todos los productos?", "Eliminar",
                JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
            if (JOptionPane.showConfirmDialog(this,
                    "Está seguro que desea eliminar TODOS los registros?", "Eliminar",
                    JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
                try {
                    String mensaje = gestorPrincipal.eliminarTODOSProductos();
                    JOptionPane.showMessageDialog(null, mensaje);
                } catch (SQLException ex) {

                }
            }
        }
    }

    public void limpiarCampos() {
        txtCantidad.setText("");
        txtID.setText("");
        txtMedida.setText("");
        txtNomProducto.setText("");
        txtPrecio.setText("");
    }

    private void deshabilitarRB() {
        bgModoFiltrar.clearSelection();
        rbTipo.setEnabled(false);
        rbNombre.setEnabled(false);
    }

    private void habilitarRB() {
        rbTipo.setEnabled(true);
        rbNombre.setEnabled(true);
    }

    public void agregar() throws SQLException {
        String mensaje;
        mensaje = gestorPrincipal.agregarProducto(new Producto(
                0,
                (String) cmbTipo.getSelectedItem(),
                txtNomProducto.getText().trim(),
                cmbTipo.getSelectedItem().equals("Herramienta") ? "(sin medida)" : txtMedida.getText().trim(),
                cmbTipo.getSelectedItem().equals("Herramienta") ? (String) cmbCapacidad.getSelectedItem() : "(sin capacidad)",
                Double.parseDouble(txtPrecio.getText().trim()),
                Integer.parseInt(txtCantidad.getText().trim())));
        JOptionPane.showMessageDialog(null, mensaje);
    }

    public void actualizar() throws SQLException {
        String mensaje;
        mensaje = gestorPrincipal.actualizarProducto(new Producto(
                Integer.parseInt(txtID.getText().trim()),
                (String) cmbTipo.getSelectedItem(),
                txtNomProducto.getText().trim(),
                cmbTipo.getSelectedItem().equals("Herramienta") ? "(sin medida)" : txtMedida.getText().trim(),
                cmbTipo.getSelectedItem().equals("Herramienta") ? (String) cmbCapacidad.getSelectedItem() : "(sin capacidad)",
                Double.parseDouble(txtPrecio.getText().trim()),
                Integer.parseInt(txtCantidad.getText().trim())));
        JOptionPane.showMessageDialog(null, mensaje);
    }

    public void buscar() throws SQLException {
        if (rbNombre.isSelected()) {
            gestorPrincipal.buscarProducto("nombre", "%" + txtNomProducto.getText().trim() + "%");
        } else if (rbTipo.isSelected()) {
            gestorPrincipal.buscarProducto("tipo", (String) cmbTipo.getSelectedItem());
        }
    }

    public void productoACampos() {
        txtID.setText(String.valueOf(productoAux.getId()));
        if (productoAux.getTipo().equals("Herramienta")) {
            cmbTipo.setSelectedIndex(0);
            cmbTipo.setSelectedItem(productoAux.getCapacidad());
        } else {
            cmbTipo.setSelectedIndex(1);
        }
        txtNomProducto.setText(productoAux.getNombre());
        txtMedida.setText(productoAux.getMedida());
        txtPrecio.setText(String.valueOf(productoAux.getPrecio()));
        txtCantidad.setText(String.valueOf(productoAux.getCantidad()));
    }

    /*  !!!!   INICIO DE CODIGO AUTOGENERADO   ¡¡¡¡*/
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgModoFiltrar = new javax.swing.ButtonGroup();
        jpBotones = new javax.swing.JPanel();
        btnAgregar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btnFacturar = new javax.swing.JButton();
        pnCriterioFiltrar = new javax.swing.JPanel();
        rbNombre = new javax.swing.JRadioButton();
        rbTipo = new javax.swing.JRadioButton();
        jpBarraEstado = new javax.swing.JPanel();
        jpCentral = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNomProducto = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtMedida = new javax.swing.JTextField();
        cmbCapacidad = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cmbTipo = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        jpTabla = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtTablaProductos = new javax.swing.JTable();
        jToolBar1 = new javax.swing.JToolBar();
        btnRefrescar = new javax.swing.JButton();
        barraMenu = new javax.swing.JMenuBar();
        menuArchivo = new javax.swing.JMenu();
        itemSalir = new javax.swing.JMenuItem();
        menuDatos = new javax.swing.JMenu();
        itemEliminarTodo = new javax.swing.JMenuItem();
        itemRefrescar = new javax.swing.JMenuItem();
        menuInformacion = new javax.swing.JMenu();
        itemCreditos = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(1000, 650));

        jpBotones.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnAgregar.setText("Agregar");
        btnAgregar.setDoubleBuffered(true);
        btnAgregar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnAgregar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnAgregar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickAgregar(evt);
            }
        });

        btnModificar.setText("Modificar");
        btnModificar.setDoubleBuffered(true);
        btnModificar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnModificar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnModificar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickModificar(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.setDoubleBuffered(true);
        btnBuscar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnBuscar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnBuscar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickBuscar(evt);
            }
        });

        btnEliminar.setForeground(new java.awt.Color(255, 0, 0));
        btnEliminar.setText("Eliminar");
        btnEliminar.setBorderPainted(false);
        btnEliminar.setDoubleBuffered(true);
        btnEliminar.setEnabled(false);
        btnEliminar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnEliminar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnEliminar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickEliminar(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.setDoubleBuffered(true);
        btnCancelar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnCancelar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnCancelar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickCancelar(evt);
            }
        });

        btnAceptar.setText("Aceptar");
        btnAceptar.setDoubleBuffered(true);
        btnAceptar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnAceptar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnAceptar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickAceptar(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Modos:");
        jLabel4.setPreferredSize(new java.awt.Dimension(83, 20));

        btnFacturar.setText("Facturar");
        btnFacturar.setDoubleBuffered(true);
        btnFacturar.setEnabled(false);
        btnFacturar.setMaximumSize(new java.awt.Dimension(82, 32));
        btnFacturar.setMinimumSize(new java.awt.Dimension(82, 32));
        btnFacturar.setPreferredSize(new java.awt.Dimension(82, 32));
        btnFacturar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickFacturar(evt);
            }
        });

        pnCriterioFiltrar.setMaximumSize(new java.awt.Dimension(88, 50));
        pnCriterioFiltrar.setMinimumSize(new java.awt.Dimension(88, 50));

        rbNombre.setText("Nombre");
        rbNombre.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbNombre.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        rbNombre.setMaximumSize(new java.awt.Dimension(82, 32));
        rbNombre.setMinimumSize(new java.awt.Dimension(82, 32));
        rbNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickNombre(evt);
            }
        });

        rbTipo.setText("Tipo");
        rbTipo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        rbTipo.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        rbTipo.setMaximumSize(new java.awt.Dimension(82, 32));
        rbTipo.setMinimumSize(new java.awt.Dimension(82, 32));
        rbTipo.setPreferredSize(new java.awt.Dimension(77, 28));
        rbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickTipo(evt);
            }
        });

        javax.swing.GroupLayout pnCriterioFiltrarLayout = new javax.swing.GroupLayout(pnCriterioFiltrar);
        pnCriterioFiltrar.setLayout(pnCriterioFiltrarLayout);
        pnCriterioFiltrarLayout.setHorizontalGroup(
            pnCriterioFiltrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnCriterioFiltrarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnCriterioFiltrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnCriterioFiltrarLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(rbNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnCriterioFiltrarLayout.createSequentialGroup()
                        .addComponent(rbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        pnCriterioFiltrarLayout.setVerticalGroup(
            pnCriterioFiltrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnCriterioFiltrarLayout.createSequentialGroup()
                .addComponent(rbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jpBotonesLayout = new javax.swing.GroupLayout(jpBotones);
        jpBotones.setLayout(jpBotonesLayout);
        jpBotonesLayout.setHorizontalGroup(
            jpBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpBotonesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFacturar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnCriterioFiltrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpBotonesLayout.setVerticalGroup(
            jpBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpBotonesLayout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnCriterioFiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(btnFacturar, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        getContentPane().add(jpBotones, java.awt.BorderLayout.LINE_END);

        jpBarraEstado.setPreferredSize(new java.awt.Dimension(641, 30));

        javax.swing.GroupLayout jpBarraEstadoLayout = new javax.swing.GroupLayout(jpBarraEstado);
        jpBarraEstado.setLayout(jpBarraEstadoLayout);
        jpBarraEstadoLayout.setHorizontalGroup(
            jpBarraEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 837, Short.MAX_VALUE)
        );
        jpBarraEstadoLayout.setVerticalGroup(
            jpBarraEstadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        getContentPane().add(jpBarraEstado, java.awt.BorderLayout.PAGE_END);

        jpCentral.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("ID");

        txtID.setEditable(false);
        txtID.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtID.setDoubleBuffered(true);
        txtID.setMargin(new java.awt.Insets(5, 0, 0, 0));

        jLabel2.setText("Nombre Producto");

        txtNomProducto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNomProducto.setDoubleBuffered(true);
        txtNomProducto.setEnabled(false);

        jLabel3.setText("Medida");

        txtMedida.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtMedida.setDoubleBuffered(true);
        txtMedida.setEnabled(false);

        cmbCapacidad.setEditable(true);
        cmbCapacidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Liviano", "Mediano", "Pesado" }));
        cmbCapacidad.setToolTipText("");
        cmbCapacidad.setBorder(null);
        cmbCapacidad.setDoubleBuffered(true);
        cmbCapacidad.setEnabled(false);

        jLabel6.setText("Precio");

        txtPrecio.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPrecio.setDoubleBuffered(true);
        txtPrecio.setEnabled(false);
        txtPrecio.setMargin(new java.awt.Insets(5, 0, 0, 0));

        jLabel7.setText("Capacidad de Trabajo");

        jLabel8.setText("Tipo");

        cmbTipo.setEditable(true);
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Herramienta", "Material" }));
        cmbTipo.setToolTipText("");
        cmbTipo.setBorder(null);
        cmbTipo.setDoubleBuffered(true);
        cmbTipo.setEnabled(false);
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cambiarSeleccion(evt);
            }
        });

        jLabel9.setText("Cantidad");

        txtCantidad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtCantidad.setDoubleBuffered(true);
        txtCantidad.setEnabled(false);
        txtCantidad.setMargin(new java.awt.Insets(5, 0, 0, 0));

        jpTabla.setLayout(new java.awt.BorderLayout());

        jtTablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jtTablaProductos);

        jpTabla.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jToolBar1.setRollover(true);

        btnRefrescar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/proyecto2/recursos/newIconoRefrescar.png"))); // NOI18N
        btnRefrescar.setFocusable(false);
        btnRefrescar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefrescar.setMaximumSize(new java.awt.Dimension(20, 20));
        btnRefrescar.setMinimumSize(new java.awt.Dimension(20, 20));
        btnRefrescar.setPreferredSize(new java.awt.Dimension(20, 20));
        btnRefrescar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickRefrescar(evt);
            }
        });
        jToolBar1.add(btnRefrescar);

        jpTabla.add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout jpCentralLayout = new javax.swing.GroupLayout(jpCentral);
        jpCentral.setLayout(jpCentralLayout);
        jpCentralLayout.setHorizontalGroup(
            jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpCentralLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jpTabla, javax.swing.GroupLayout.DEFAULT_SIZE, 693, Short.MAX_VALUE)
                    .addGroup(jpCentralLayout.createSequentialGroup()
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtID)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbTipo, 0, 99, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNomProducto))
                        .addGap(18, 18, 18)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMedida, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbCapacidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPrecio))
                        .addGap(18, 18, 18)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCantidad))))
                .addGap(16, 16, 16))
        );
        jpCentralLayout.setVerticalGroup(
            jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpCentralLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jpCentralLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpCentralLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtID))
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jpCentralLayout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jpCentralLayout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jpCentralLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNomProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jpCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbCapacidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpTabla, javax.swing.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jpCentral, java.awt.BorderLayout.CENTER);

        menuArchivo.setText("Archivo");

        itemSalir.setText("Salir");
        itemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirAplicacion(evt);
            }
        });
        menuArchivo.add(itemSalir);

        barraMenu.add(menuArchivo);

        menuDatos.setText("Datos");

        itemEliminarTodo.setText("Eliminar Todo");
        itemEliminarTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarTodo(evt);
            }
        });
        menuDatos.add(itemEliminarTodo);

        itemRefrescar.setText("Refrescar Tabla");
        itemRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickRefrescar(evt);
            }
        });
        menuDatos.add(itemRefrescar);

        barraMenu.add(menuDatos);

        menuInformacion.setText("Información");

        itemCreditos.setText("Creditos");
        itemCreditos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickCreditos(evt);
            }
        });
        menuInformacion.add(itemCreditos);

        barraMenu.add(menuInformacion);

        setJMenuBar(barraMenu);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc=" Metodos generados por el edito de formulario ">

    private void salirAplicacion(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirAplicacion
        cerrarAplicacion();
    }//GEN-LAST:event_salirAplicacion

    private void clickCreditos(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickCreditos
        creditos();
    }//GEN-LAST:event_clickCreditos

    private void clickAgregar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickAgregar

        habilitarCampos();
        habilitarBotones();
        btnAgregar.setEnabled(false);
        btnAceptar.setText("Guardar");
        modo = "agregar";
        limpiarCampos();

    }//GEN-LAST:event_clickAgregar

    private void clickModificar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickModificar

        btnModificar.setEnabled(false);
        btnAceptar.setText("Actualizar");
        modo = "actualizar";
        limpiarCampos();

    }//GEN-LAST:event_clickModificar

    private void clickBuscar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickBuscar

        habilitarBotones();
        deshabilitarCampos();
        btnBuscar.setEnabled(false);
        habilitarRB();
        btnAceptar.setText("Filtrar");
        modo = "buscar";
        limpiarCampos();

    }//GEN-LAST:event_clickBuscar

    private void clickEliminar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickEliminar

        String mensaje = "";
        if (JOptionPane.showConfirmDialog(this,
                "Desea eliminar este producto?", "Eliminar",
                JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
            try {
                mensaje = gestorPrincipal.eliminarProducto(Integer.parseInt((String) modeloTP.getValueAt(filaSelecionada, 0)));
                JOptionPane.showMessageDialog(null, mensaje);
                btnEliminar.setEnabled(false);
            } catch (SQLException ex) {
                Logger.getLogger(Ferreteria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        

    }//GEN-LAST:event_clickEliminar

    private void clickAceptar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickAceptar

        try {
            switch (modo) {
                case "agregar":
                    agregar();
                    break;
                case "actualizar":
                    actualizar();
                    break;
                case "buscar":
                    buscar();
                    break;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Ferreteria.class.getName()).log(Level.SEVERE, null, ex);
        }
        habilitarBotones();
        deshabilitarCampos();
        deshabilitarRB();
        btnAceptar.setText("Aceptar");
        modo = "espera";
        limpiarCampos();

    }//GEN-LAST:event_clickAceptar

    private void clickCancelar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickCancelar

        habilitarBotones();
        deshabilitarCampos();
        deshabilitarRB();
        btnAceptar.setText("Aceptar");
        modo = "espera";
        limpiarCampos();

    }//GEN-LAST:event_clickCancelar

    private void clickFacturar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickFacturar

        if (!gestorPrincipal.getFacturando()) {
            ventanaFacturar = new Facturar(gestorPrincipal, this);
            ventanaFacturar.inicializar();
            gestorPrincipal.setFacturando(true);
        }

        ventanaFacturar.agregarDetalle(new DetalleFactura(
                productoAux.getNombre(),
                productoAux.getPrecio()));
        
        ventanaFacturar.toFront();

    }//GEN-LAST:event_clickFacturar

    private void cambiarSeleccion(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cambiarSeleccion

        if (cmbTipo.getSelectedItem().equals("Herramienta")) {
            txtMedida.setEnabled(false);
            cmbCapacidad.setEnabled(true);
        } else if (cmbTipo.getSelectedItem().equals("Material")) {
            txtMedida.setEnabled(true);
            cmbCapacidad.setEnabled(false);
        }

    }//GEN-LAST:event_cambiarSeleccion

    private void eliminarTodo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarTodo
        eliminarTODOSProductos();
    }//GEN-LAST:event_eliminarTodo

    private void clickTipo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickTipo
        cmbTipo.setEnabled(true);
        txtNomProducto.setEnabled(false);
    }//GEN-LAST:event_clickTipo

    private void clickNombre(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickNombre
        cmbTipo.setEnabled(false);
        txtNomProducto.setEnabled(true);
    }//GEN-LAST:event_clickNombre

    private void clickRefrescar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickRefrescar
        try {
            gestorPrincipal.refrescarDatos();
        } catch (SQLException ex) {
        }
    }//GEN-LAST:event_clickRefrescar

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Declaracion de variables del editor de formulario ">

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barraMenu;
    private javax.swing.ButtonGroup bgModoFiltrar;
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnFacturar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnRefrescar;
    private javax.swing.JComboBox<String> cmbCapacidad;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JMenuItem itemCreditos;
    private javax.swing.JMenuItem itemEliminarTodo;
    private javax.swing.JMenuItem itemRefrescar;
    private javax.swing.JMenuItem itemSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JPanel jpBarraEstado;
    private javax.swing.JPanel jpBotones;
    private javax.swing.JPanel jpCentral;
    private javax.swing.JPanel jpTabla;
    private javax.swing.JTable jtTablaProductos;
    private javax.swing.JMenu menuArchivo;
    private javax.swing.JMenu menuDatos;
    private javax.swing.JMenu menuInformacion;
    private javax.swing.JPanel pnCriterioFiltrar;
    private javax.swing.JRadioButton rbNombre;
    private javax.swing.JRadioButton rbTipo;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtMedida;
    private javax.swing.JTextField txtNomProducto;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc=" Metodo main del formulario ">
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ferreteria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ferreteria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ferreteria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ferreteria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new Ferreteria("Poh eso").setVisible(true);
            }
        });
    }

    // </editor-fold>    
}
