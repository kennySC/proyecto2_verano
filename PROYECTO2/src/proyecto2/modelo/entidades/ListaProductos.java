/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.entidades;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Kenneth Sibaja
 */
public class ListaProductos extends Observable {
    
    private List<Producto> listaProductos;

    public ListaProductos() {
        listaProductos = new ArrayList<>();
    }        
    
    public ListaProductos(List<Producto> lista){
        listaProductos = lista;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public int cantidadProductos() {
        return listaProductos.size();
    }

    public Producto obtenerProducto(int i) {
        return listaProductos.get(i);
    }

    public void borrar() {
        listaProductos.clear();
        setChanged();
        notifyObservers();
    }

    public void agregar(Producto t) {
        listaProductos.add(t);
        setChanged();
        notifyObservers(t);
    }
    
    public boolean esVacia(){
        return listaProductos.isEmpty();
    }
    
    public void imprimirLista(){
        if (!listaProductos.isEmpty()) {
            listaProductos.forEach((t) -> {
                System.out.println(t.toString());
            });
        } else {
            System.out.println("Lista de Productos vacia.");
        }
    }
    
    public ListaProductos obtenerModelo() {
        return this;
    }
    
}
