package proyecto2.modelo.entidades;

import java.io.Serializable;

/**
 *
 * @author Kenneth Sibaja
 */
public class Producto implements Serializable {

    private int id;
    private String tipo;
    private String nombre;
    private String medida;
    private String capacidad;
    private double precio;
    private int cantidad;

    public Producto(int id, String tipo, String nombre, String medida, String capacidad, double precio, int cantidad) {
        this.id = id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.medida = medida;
        this.capacidad = capacidad;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public Object[] toArray() {
        Object[] r = new Object[7];
        r[0] = getId();
        r[1] = getTipo();
        r[2] = getNombre();
        r[3] = getMedida();
        r[4] = getCapacidad();
        r[5] = getPrecio();
        r[6] = getCantidad();
        return r;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public static int getFieldCount() {
        return 7;
    }

    @Override
    public String toString() {
        return "Producto{ " + nombre + " " + tipo + " " + cantidad + " };";
    }
    
    

}
