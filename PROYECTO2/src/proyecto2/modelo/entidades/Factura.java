/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.entidades;

/**
 *
 * @author Kenneth Sibaja
 */
public class Factura {
    
    private int id;
    private String nombreCliente;
    private double precioTotal;

    public Factura(int id, String nombreCliente, double precioTotal) {
        this.id = id;
        this.nombreCliente = nombreCliente;
        this.precioTotal = precioTotal;
    }
    
    public Object[] toArray() {
        Object[] r = new Object[3];
        r[0] = getId();
        r[1] = getNombreCliente();
        r[2] = getPrecioTotal();
        return r;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }
            
}
