/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.entidades;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Kenneth Sibaja
 */
public class ListaFacturas extends Observable{
    
    private List<Factura> listaFacturas;

    public ListaFacturas() {
        listaFacturas = new ArrayList<>();
    }        
    
    public ListaFacturas(List<Factura> lista){
        listaFacturas = lista;
    }

    public List<Factura> getListaFacturas() {
        return listaFacturas;
    }

    public void setListaFacturas(List<Factura> listaFacturas) {
        this.listaFacturas = listaFacturas;
    }

    public int cantidadFacturas() {
        return listaFacturas.size();
    }

    public Factura obtenerFactura(int i) {
        return listaFacturas.get(i);
    }

    public void borrar() {
        listaFacturas.clear();
        setChanged();
        notifyObservers();
    }

    public void agregar(Factura t) {
        listaFacturas.add(t);
        setChanged();
        notifyObservers(t);
    }
    
    public boolean esVacia(){
        return listaFacturas.isEmpty();
    }
    
    public void imprimirLista(){
        if (!listaFacturas.isEmpty()) {
            listaFacturas.forEach((t) -> {
                System.out.println(t.toString());
            });
        } else {
            System.out.println("Lista de Facturas vacia.");
        }
    }
    
    public ListaFacturas obtenerModelo() {
        return this;
    }
    
}
