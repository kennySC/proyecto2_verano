/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.entidades;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 *
 * @author Kenneth Sibaja
 */
public class ListaDetallesFactura extends Observable{
    
    private List<DetalleFactura> listaDetalleFacturas;

    public ListaDetallesFactura() {
        listaDetalleFacturas = new ArrayList<>();
    }        
    
    public ListaDetallesFactura(List<DetalleFactura> lista){
        listaDetalleFacturas = lista;
    }

    public List<DetalleFactura> getListaDetalleFacturas() {
        return listaDetalleFacturas;
    }

    public void setListaDetalleFacturas(List<DetalleFactura> listaDetalleFacturas) {
        this.listaDetalleFacturas = listaDetalleFacturas;
    }

    public int cantidadDetalleFacturas() {
        return listaDetalleFacturas.size();
    }

    public DetalleFactura obtenerDetalleFactura(int i) {
        return listaDetalleFacturas.get(i);
    }

    public void borrar() {
        listaDetalleFacturas.clear();
        setChanged();
        notifyObservers();
    }

    public void agregar(DetalleFactura t) {
        listaDetalleFacturas.add(t);
        setChanged();
        notifyObservers(t);
    }
    
    public void eliminarElemento(DetalleFactura df){
        if(listaDetalleFacturas.contains(df)){
            listaDetalleFacturas.remove(df);
        }
    }
    
    public boolean esVacia(){
        return listaDetalleFacturas.isEmpty();
    }
    
    public void imprimirLista(){
        if (!listaDetalleFacturas.isEmpty()) {
            listaDetalleFacturas.forEach((t) -> {
                System.out.println(t.toString());
            });
        } else {
            System.out.println("Lista de DetalleFacturas vacia.");
        }
    }
    
    public ListaDetallesFactura obtenerModelo() {
        return this;
    }
}
