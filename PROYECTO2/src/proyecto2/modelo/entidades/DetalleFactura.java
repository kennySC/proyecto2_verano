/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.entidades;

/**
 *
 * @author Kenneth Sibaja
 */
public class DetalleFactura {
    
    private int id;
    private int fk_idFactura;
    private String nombreProducto;
    private double precio;

    public DetalleFactura(int id, int fk_idFactura, String nombreProducto, double precio) {
        this.id = id;
        this.fk_idFactura = fk_idFactura;
        this.nombreProducto = nombreProducto;
        this.precio = precio;
    }
    
    public DetalleFactura(String nombre, double precio){
        this.nombreProducto = nombre;
        this.precio = precio;
    }
    
    public Object[] toArray() {
        Object[] r = new Object[2];
        r[0] = getNombreProducto();
        r[1] = getPrecio();
        return r;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFk_idFactura() {
        return fk_idFactura;
    }

    public void setFk_idFactura(int fk_idFactura) {
        this.fk_idFactura = fk_idFactura;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
    
}
