package proyecto2.modelo;

import java.sql.SQLException;
import java.util.Observable;
import proyecto2.modelo.DAO.ProductoDAO;
import proyecto2.modelo.entidades.ListaProductos;
import proyecto2.modelo.entidades.Producto;

/**
 *
 * @author Matthew Miranda
 */
public class ModeloProductos extends Observable {

    private ListaProductos listaProductos;

    public ModeloProductos() throws SQLException {
        listaProductos = new ListaProductos(ProductoDAO.obtenerInstancia().listar());
    }

    private void notificarCambios() throws SQLException {
        listaProductos.setListaProductos(ProductoDAO.obtenerInstancia().listar());
        setChanged();
        notifyObservers(listaProductos);
    }
    
    public ListaProductos obtenerLista() {
        return listaProductos;
    }
    
    public String agregarProducto(Producto p) throws SQLException {
        String mensaje;
        if (ProductoDAO.obtenerInstancia().agregar(p)) {
            notificarCambios();
            return "Producto agregado correctamente";
        } else {
            return "Error al agegar el producto";
        }
    }   

    public String eliminarRegistro(int id) throws SQLException {
        String mensaje;
        if (ProductoDAO.obtenerInstancia().eliminarSeleccionado(id)) {
            notificarCambios();
            return "Producto eliminado correctamente";
        }
        return "Error al eliminado el producto";
    }

    public String actualizarProducto(Producto p) throws SQLException {
        String mensaje;
        if (ProductoDAO.obtenerInstancia().actualizar(p)) {
            notificarCambios();
            return "Producto actualizado correctamente";
        }
        return "Error al actualizar el producto";
    }

    public String eliminarTODOS() throws SQLException {
        if(ProductoDAO.obtenerInstancia().eliminarTodos()){
            notificarCambios();
            return "Todos los registros han sido eliminados correctamente";
        }
        return "Error al eliminar los registros";
    }
    
    public void buscar(String criterio, String parametro) throws SQLException{
        listaProductos.setListaProductos(ProductoDAO.obtenerInstancia().buscar(criterio, parametro));
        setChanged();
        notifyObservers(listaProductos);
    }

    public void refrescarDatos() throws SQLException {
        //  Solo debe llamarse este metodo porque notificarcambios 
        //  ya lista todo el contenido de la base de datos
        notificarCambios();
    }
    
}
