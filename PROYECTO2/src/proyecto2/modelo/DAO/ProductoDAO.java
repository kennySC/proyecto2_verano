package proyecto2.modelo.DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Properties;
import proyecto2.modelo.entidades.ListaProductos;
import proyecto2.modelo.entidades.Producto;

/**
 *
 * @author Kenneth Sibaja
 */
public class ProductoDAO extends Observable {

    // <editor-fold defaultstate="collapsed" desc=" comandos para los diferentes mantenimientos ">
    private static final String CMD_LISTAR = "SELECT * FROM producto;";
    private static final String CMD_AGREGAR
            = "INSERT INTO producto (tipo, nombre, medida, capacidad, precio, cantidad) "
            + "VALUES (?, ?, ?, ?, ?, ?);";
    private static final String CMD_ELIMINAR = "DELETE FROM producto WHERE id = ?;";
    private static final String CMD_ACTUALIZAR
            = "UPDATE producto SET tipo = ?, nombre = ?, medida = ?, capacidad = ?, precio = ?, cantidad = ? "
            + "WHERE id = ?;";
    private static final String CMD_ELIMINARTODOS = "DELETE FROM producto;";
    private static final String CMD_FILTRAR_NOMBRE = "SELECT * FROM producto WHERE nombre like ?;";
    private static final String CMD_FILTRAR_TIPO = "SELECT * FROM producto WHERE tipo = ?;";
    // <editor-fold>

    private static ProductoDAO instancia = null;

    private Properties cfg;
    private String baseDatos;
    private String usuario;
    private String clave;

    private ProductoDAO() {
        this.cfg = new Properties();
        try {
            this.cfg.load(getClass().getResourceAsStream("configuracion.properties"));
            this.baseDatos = cfg.getProperty("base_datos");
            this.usuario = cfg.getProperty("usuario");
            this.clave = cfg.getProperty("clave");
        } catch (IOException ex) {
            System.err.printf("Excepción: '%s'%n", ex.getMessage());
        }
    }

    public static ProductoDAO obtenerInstancia() {
        if (instancia == null) {
            instancia = new ProductoDAO();
        }
        return instancia;
    }

    private Connection obtenerConexion() throws SQLException {
        return GestorBD.obtenerInstancia().obtenerConexion(baseDatos, usuario, clave);
    }

    public List<Producto> listar() throws SQLException {
        List<Producto> r = new ArrayList<>();

        try (Connection cnx = obtenerConexion();
                Statement stm = cnx.createStatement();
                ResultSet rs = stm.executeQuery(CMD_LISTAR)) {

            while (rs.next()) {
                r.add(new Producto(
                        rs.getInt("id"),
                        rs.getString("tipo"),
                        rs.getString("nombre"),
                        rs.getString("medida"),
                        rs.getString("capacidad"),
                        rs.getDouble("precio"),
                        rs.getInt("cantidad")
                ));
            }
        }
        return r;
    }

    public boolean agregar(Producto p) throws SQLException {
        boolean exito = false;
        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_AGREGAR)) {

            stm.clearParameters();
            stm.setString(1, p.getTipo());
            stm.setString(2, p.getNombre());
            stm.setString(3, p.getMedida());
            stm.setString(4, p.getCapacidad());
            stm.setDouble(5, p.getPrecio());
            stm.setInt(6, p.getCantidad());

            exito = stm.executeUpdate() == 1;

        }

        return exito;
    }

    public List<Producto> buscar(String criterio, String parametro) throws SQLException {
        List<Producto> r = new ArrayList<>();
        try (Connection cnx = obtenerConexion()) {
            PreparedStatement stm;
            if(criterio.equalsIgnoreCase("nombre")){                            
                stm = cnx.prepareStatement(CMD_FILTRAR_NOMBRE);
            }else{
                stm = cnx.prepareStatement(CMD_FILTRAR_TIPO);
            }
            
            stm.clearParameters();
            stm.setString(1, parametro);
            
            ResultSet rs = stm.executeQuery();
            
            while(rs.next()){
                r.add(new Producto(
                        rs.getInt("id"),
                        rs.getString("tipo"),
                        rs.getString("nombre"),
                        rs.getString("medida"),
                        rs.getString("capacidad"),
                        rs.getDouble("precio"),
                        rs.getInt("cantidad")
                ));
            }
            
        }
        return r;
    }

    public boolean actualizar(Producto p) throws SQLException {
        boolean exito = false;
        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_ACTUALIZAR)) {
            stm.clearParameters();
            stm.setString(1, p.getTipo());
            stm.setString(2, p.getNombre());
            stm.setString(3, p.getMedida());
            stm.setString(4, p.getCapacidad());
            stm.setDouble(5, p.getPrecio());
            stm.setInt(6, p.getCantidad());
            stm.setInt(7, p.getId());

            exito = stm.executeUpdate() == 1;
        }
        return exito;
    }

    public boolean eliminarSeleccionado(int id) throws SQLException {
        boolean exito = false;
        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_ELIMINAR)) {
            stm.clearParameters();
            stm.setInt(1, id);

            exito = stm.executeUpdate() == 1;
        }
        return exito;
    }

    public boolean eliminarTodos() throws SQLException {
        boolean exito = false;
        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_ELIMINARTODOS)) {
            stm.clearParameters();

            exito = stm.executeUpdate() != 0;
        }
        return exito;
    }

    @Override
    public String toString() {
        StringBuilder r = new StringBuilder("[\n");
        try {
            List<Producto> productos = listar();
            for (Producto e : productos) {
                r.append(String.format("\t%s,%n", e));
            }
        } catch (SQLException ex) {
            r.append(String.format("(Excepción: '%s')", ex.getMessage()));
        }
        r.append("]");
        return r.toString();
    }

}
