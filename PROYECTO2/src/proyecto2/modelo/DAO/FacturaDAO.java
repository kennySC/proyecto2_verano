/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.DAO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import proyecto2.modelo.entidades.DetalleFactura;
import proyecto2.modelo.entidades.Factura;

/**
 *
 * @author Kenneth Sibaja
 */
public class FacturaDAO {

    private static final String CMD_LISTARFACTURAS = "SELECT * FROM factura;";
    private static final String CMD_AGREGARFACTURA = "INSERT INTO factura (nombre_cliente, precio_total)"
            + "VALUES (?, ?)";
    private static final String CMD_FILTRAR_FACTURA_NOMBRE = "SELECT * FROM fatura WHERE nombre_cliente like ?;";
    private static final String CMD_AGREGARDETALLE = "INSERT INTO detalle_factura (fk_id_factura, nombre_producto, precio)"
            + "VALUES (?, ?, ?)";
    private static final String CMD_ULTIMOID = "SELECT @last_id := MAX(id) FROM factura;";

    private static FacturaDAO instancia = null;

    private Properties cfg;
    private String baseDatos;
    private String usuario;
    private String clave;

    private FacturaDAO() {
        this.cfg = new Properties();
        try {
            this.cfg.load(getClass().getResourceAsStream("configuracion.properties"));
            this.baseDatos = cfg.getProperty("base_datos");
            this.usuario = cfg.getProperty("usuario");
            this.clave = cfg.getProperty("clave");
        } catch (IOException ex) {
            System.err.printf("Excepción: '%s'%n", ex.getMessage());
        }
    }

    public static FacturaDAO obtenerInstancia() {
        if (instancia == null) {
            instancia = new FacturaDAO();
        }
        return instancia;
    }

    private Connection obtenerConexion() throws SQLException {
        return GestorBD.obtenerInstancia().obtenerConexion(baseDatos, usuario, clave);
    }

    public List<Factura> listar() throws SQLException {
        List<Factura> r = new ArrayList<>();

        try (Connection cnx = obtenerConexion();
                Statement stm = cnx.createStatement();
                ResultSet rs = stm.executeQuery(CMD_LISTARFACTURAS)) {

            while (rs.next()) {
                r.add(new Factura(
                        rs.getInt("id"),
                        rs.getString("nombre_cliente"),
                        rs.getDouble("precio_total")
                ));
            }
        }
        return r;
    }

    public int agregarFactura(Factura f) throws SQLException {
        int id = 0;
        boolean exito;
        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_AGREGARFACTURA)) {

            stm.clearParameters();
            stm.setString(1, f.getNombreCliente());
            stm.setDouble(2, f.getPrecioTotal());

            exito = stm.executeUpdate() == 1;
            if (exito) {
                ResultSet rs = stm.executeQuery(CMD_ULTIMOID);
                while(rs.next()){
                    id = rs.getInt(1);
                }
            }
        }
        return id;
    }

    public boolean agregarDetalle(DetalleFactura df) throws SQLException {
        boolean exito = false;
        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_AGREGARDETALLE)) {

            stm.clearParameters();
            stm.setInt(1, df.getFk_idFactura());
            stm.setString(2, df.getNombreProducto());
            stm.setDouble(3, df.getPrecio());

            exito = stm.executeUpdate() == 1;

        }

        return exito;
    }

    public List<Factura> buscarFactura(String nombre) throws SQLException {
        List<Factura> r = new ArrayList<>();

        try (Connection cnx = obtenerConexion();
                PreparedStatement stm = cnx.prepareStatement(CMD_FILTRAR_FACTURA_NOMBRE)) {

            stm.clearParameters();
            stm.setString(1, nombre);

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                r.add(new Factura(
                        rs.getInt("id"),
                        rs.getString("nombre_cliente"),
                        rs.getDouble("precio_total")
                ));
            }
        }
        return r;
    }

}
