package proyecto2.modelo.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestorBD {
    
    private static GestorBD instancia = null;    
    
    private GestorBD() {
        System.out.println("Inicializando manejador BD..");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException
                | IllegalAccessException
                | InstantiationException ex) {
            System.err.printf("Excepción: '%s'%n", ex.getMessage());
        }
    }

    public static GestorBD obtenerInstancia() {
        if (instancia == null) {
            instancia = new GestorBD();
        }
        return instancia;
    }

    public Connection obtenerConexion(String baseDatos, String usuario, String clave) {
        try {
            Connection cnx;
            String URL_conexion
                    = String.format("%s//%s/%s%s",
                            "jdbc:mysql:", "localhost", baseDatos, "?serverTimezone=UTC");
            
            cnx = DriverManager.getConnection(URL_conexion, usuario, clave);
            return cnx;
        } catch (SQLException ex) {
            Logger.getLogger(GestorBD.class.getName()).log(Level.SEVERE, null, ex);
            return  null;
        }
    }    
    
}
