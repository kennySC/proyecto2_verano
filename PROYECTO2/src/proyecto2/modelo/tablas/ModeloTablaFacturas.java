/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.tablas;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;
import proyecto2.modelo.ModeloFacturas;
import proyecto2.modelo.ModeloProductos;
import proyecto2.modelo.entidades.ListaFacturas;
import proyecto2.modelo.entidades.Producto;

/**
 *
 * @author Kenneth Sibaja
 */
public class ModeloTablaFacturas extends AbstractTableModel implements Observer {

    private ListaFacturas facturas;
    private final DecimalFormat fmtDecimales = new DecimalFormat("#,##0.00");
    private final DecimalFormat fmtEnteros = new DecimalFormat("#0");
//    

    public ModeloTablaFacturas(ListaFacturas lista) {
        facturas = lista;
        System.out.println("Modelo de tabla creado");
        imprimirLista();
    }  

    @Override
    public int getRowCount() {
        return facturas.cantidadFacturas();
    }

    @Override
    public int getColumnCount() {
        return Producto.getFieldCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object r = facturas.obtenerFactura(rowIndex).toArray()[columnIndex];
        if (r instanceof Double) {
            r = fmtDecimales.format(r);
        }
        if (r instanceof Integer) {
            r = fmtEnteros.format(r);
        }
        return r;
    }

    public void actualizar() {
        fireTableDataChanged();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o.getClass().equals(ModeloFacturas.class)) {
            facturas = (ListaFacturas) arg;
            actualizar();
        }
    }

    public void imprimirLista() {
        facturas.imprimirLista();
    }

}
