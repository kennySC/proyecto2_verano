/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.tablas;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;
import proyecto2.modelo.ModeloProductos;
import proyecto2.modelo.entidades.ListaProductos;
import proyecto2.modelo.entidades.Producto;

/**
 *
 * @author Kenneth Sibaja
 */
public class ModeloTablaProductos extends AbstractTableModel implements Observer {

    private ListaProductos productos;
    private final DecimalFormat fmtDecimales = new DecimalFormat("#,##0.00");
    private final DecimalFormat fmtEnteros = new DecimalFormat("#0");
//    

    public ModeloTablaProductos(ListaProductos lista) {
        productos = lista;
        System.out.println("Modelo de tabla creado");
        imprimirLista();
    }  

    @Override
    public int getRowCount() {
        return productos.cantidadProductos();
    }

    @Override
    public int getColumnCount() {
        return Producto.getFieldCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object r = productos.obtenerProducto(rowIndex).toArray()[columnIndex];
        if (r instanceof Double) {
            r = fmtDecimales.format(r);
        }
        if (r instanceof Integer) {
            r = fmtEnteros.format(r);
        }
        return r;
    }    
    
    public Producto getEntidadEnFila(int fila){
        return productos.obtenerProducto(fila);
    }
    
    public void actualizar() {
        fireTableDataChanged();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o.getClass().equals(ModeloProductos.class)) {
            productos = (ListaProductos) arg;
            actualizar();
        }
    }

    public void imprimirLista() {
        productos.imprimirLista();
    }

}
