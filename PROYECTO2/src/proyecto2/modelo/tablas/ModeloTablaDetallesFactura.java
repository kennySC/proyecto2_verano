/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo.tablas;

import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;
import proyecto2.modelo.ModeloFacturas;
import proyecto2.modelo.ModeloProductos;
import proyecto2.modelo.entidades.DetalleFactura;
import proyecto2.modelo.entidades.ListaDetallesFactura;
import proyecto2.modelo.entidades.ListaProductos;
import proyecto2.modelo.entidades.Producto;

/**
 *
 * @author Kenneth Sibaja
 */
public class ModeloTablaDetallesFactura extends AbstractTableModel implements Observer {

    private ListaDetallesFactura detalles;
    private final DecimalFormat fmtDecimales = new DecimalFormat("#,##0.00");
    private final DecimalFormat fmtEnteros = new DecimalFormat("#0");
//    

    public ModeloTablaDetallesFactura(ListaDetallesFactura lista) {
        detalles = lista;
        System.out.println("Modelo de tabla creado");
        imprimirLista();
    }  

    @Override
    public int getRowCount() {
        return detalles.cantidadDetalleFacturas();
    }

    @Override
    public int getColumnCount() {
        return Producto.getFieldCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object r = detalles.obtenerDetalleFactura(rowIndex).toArray()[columnIndex];
        if (r instanceof Double) {
            r = fmtDecimales.format(r);
        }
        if (r instanceof Integer) {
            r = fmtEnteros.format(r);
        }
        return r;
    }
    
    public DetalleFactura getEntidadEnFila(int fila){
        return detalles.obtenerDetalleFactura(fila);
    }

    public void actualizar() {
        fireTableDataChanged();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o.getClass().equals(ModeloFacturas.class)) {
            if (arg.getClass().equals(ListaDetallesFactura.class)) {
                detalles = (ListaDetallesFactura) arg;
                actualizar();
            }
        }
    }

    public void imprimirLista() {
        detalles.imprimirLista();
    }

}
