/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2.modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecto2.modelo.DAO.FacturaDAO;
import proyecto2.modelo.entidades.DetalleFactura;
import proyecto2.modelo.entidades.Factura;
import proyecto2.modelo.entidades.ListaDetallesFactura;
import proyecto2.modelo.entidades.ListaFacturas;

/**
 *
 * @author Kenneth Sibaja
 */
public class ModeloFacturas extends Observable {

    private ListaFacturas listaFacturas;
    private ListaDetallesFactura listaDetalles;
    private boolean facturando;

    public ModeloFacturas() throws SQLException {
        this.listaFacturas = new ListaFacturas(FacturaDAO.obtenerInstancia().listar());
        this.listaDetalles = new ListaDetallesFactura();
    }

    public ListaFacturas obtenerFacturas() {
        return listaFacturas;
    }

    public ListaDetallesFactura obtenerDetalles() {
        return listaDetalles;
    }

    public int agregarFactura(Factura f) throws SQLException {
        int i = FacturaDAO.obtenerInstancia().agregarFactura(f);
        notificarCambiosFacturas();
        return i;
    }

    public void agregarDetalleFatura(DetalleFactura df) {
        listaDetalles.agregar(df);
        notificarCambiosDetalles();
    }

    public boolean guadarDetallesFactura(int id_llaveForanea) throws SQLException {                        
        for (DetalleFactura df : listaDetalles.getListaDetalleFacturas()) {
            df.setFk_idFactura(id_llaveForanea);
            if(!FacturaDAO.obtenerInstancia().agregarDetalle(df)){
                return false;
            }
        }        
        return true;
    }

    public void buscarFactura(String nombre) throws SQLException {
        listaFacturas.setListaFacturas(FacturaDAO.obtenerInstancia().buscarFactura(nombre));
        setChanged();
        notifyObservers(listaFacturas);
    }

    public void notificarCambiosFacturas() throws SQLException {
        listaFacturas.setListaFacturas(FacturaDAO.obtenerInstancia().listar());
        setChanged();
        notifyObservers(listaFacturas);
    }

    public void notificarCambiosDetalles() {
        setChanged();
        notifyObservers(listaDetalles);
    }

    public void quitarDetalle(DetalleFactura df) {
        listaDetalles.eliminarElemento(df);
        notificarCambiosDetalles();
    }

    public double obtenerPrecioTotal() {
        double precioT = 0.0;
        for (DetalleFactura df : listaDetalles.getListaDetalleFacturas()) {           
            precioT += df.getPrecio();
        }
        return precioT;
    }
    
    public void setFacturando(boolean b) {
        this.facturando = b;
    }
    
    public boolean getFacturando(){
        return facturando;
    }

    public void limpiar() {
        listaDetalles.borrar();
        listaFacturas.borrar();
    }
    
}
